
from sklearn.neighbors import KNeighborsClassifier
import time
import json
import tracemalloc
from sklearn.pipeline import Pipeline
import psutil
from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import MaxAbsScaler
from sklearn.preprocessing import StandardScaler

from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import SVC
from sklearn.linear_model import LogisticRegression
from sklearn.neural_network import MLPClassifier

from sklearn.model_selection import StratifiedKFold
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import cross_val_predict
from sklearn.model_selection import cross_val_score

from sklearn import metrics
from sklearn.metrics import confusion_matrix
from sklearn.metrics import cohen_kappa_score
from sklearn.model_selection import train_test_split

import matplotlib.pyplot as plt
from matplotlib.pyplot import figure

import numpy as np
import pandas as pd

#from scalene import scalene_profiler

header = ['Acc', 'Arg']
row = ['StandardScaler', 'MinMaxScaler', 'MaxAbsScaler']

temp_data = pd.read_csv('C:/Users/Franck/Downloads/Result/temp_data.csv')

NAIVE_FIFO_BEST = pd.read_csv('C:/Users/Franck/Downloads/Result/NAIVE_FIFO_BEST.csv')
NAIVE_MANUAL_BEST = pd.read_csv('C:/Users/Franck/Downloads/Result/NAIVE_MANUAL_BEST.csv')
NAIVE_CP8_BEST = pd.read_csv('C:/Users/Franck/Downloads/Result/NAIVE_CP8_BEST.csv')
NAIVE_PRE_BEST = pd.read_csv('C:/Users/Franck/Downloads/Result/NAIVE_PRE_BEST.csv')
NAIVE = [NAIVE_FIFO_BEST,NAIVE_MANUAL_BEST,NAIVE_CP8_BEST,NAIVE_PRE_BEST]

LR_FIFO_BEST = pd.read_csv('C:/Users/Franck/Downloads/Result/LR_FIFO_BEST.csv')
LR_MANUAL_BEST = pd.read_csv('C:/Users/Franck/Downloads/Result/LR_MANUAL_BEST.csv')
LR_CP8_BEST = pd.read_csv('C:/Users/Franck/Downloads/Result/LR_CP8_BEST.csv')
LR_PRE_BEST = pd.read_csv('C:/Users/Franck/Downloads/Result/LR_PRE_BEST.csv')
LR = [LR_FIFO_BEST,LR_MANUAL_BEST,LR_CP8_BEST,LR_PRE_BEST]
      
SVM_FIFO_BEST = pd.read_csv('C:/Users/Franck/Downloads/Result/SVM_FIFO_BEST.csv')
SVM_MANUAL_BEST = pd.read_csv('C:/Users/Franck/Downloads/Result/SVM_MANUAL_BEST.csv')
SVM_CP8_BEST = pd.read_csv('C:/Users/Franck/Downloads/Result/SVM_CP8_BEST.csv')
SVM_PRE_BEST = pd.read_csv('C:/Users/Franck/Downloads/Result/SVM_PRE_BEST.csv')
SVM = [SVM_FIFO_BEST,SVM_MANUAL_BEST,SVM_CP8_BEST,SVM_PRE_BEST]
       
KNN_FIFO_BEST = pd.read_csv('C:/Users/Franck/Downloads/Result/KNN_FIFO_BEST.csv')
KNN_MANUAL_BEST = pd.read_csv('C:/Users/Franck/Downloads/Result/KNN_MANUAL_BEST.csv')
KNN_CP8_BEST = pd.read_csv('C:/Users/Franck/Downloads/Result/KNN_CP8_BEST.csv')
KNN_PRE_BEST = pd.read_csv('C:/Users/Franck/Downloads/Result/KNN_PRE_BEST.csv')
KNN = [KNN_FIFO_BEST,KNN_MANUAL_BEST,KNN_CP8_BEST,KNN_PRE_BEST]
       
DT_FIFO_BEST = pd.read_csv('C:/Users/Franck/Downloads/Result/DT_FIFO_BEST.csv')
DT_MANUAL_BEST = pd.read_csv('C:/Users/Franck/Downloads/Result/DT_MANUAL_BEST.csv')
DT_CP8_BEST = pd.read_csv('C:/Users/Franck/Downloads/Result/DT_CP8_BEST.csv')
DT_PRE_BEST = pd.read_csv('C:/Users/Franck/Downloads/Result/DT_PRE_BEST.csv')
DT = [DT_FIFO_BEST,DT_MANUAL_BEST,DT_CP8_BEST,DT_PRE_BEST]
      
RF_FIFO_BEST = pd.read_csv('C:/Users/Franck/Downloads/Result/RF_FIFO_BEST.csv')
RF_MANUAL_BEST = pd.read_csv('C:/Users/Franck/Downloads/Result/RF_MANUAL_BEST.csv')
RF_CP8_BEST = pd.read_csv('C:/Users/Franck/Downloads/Result/RF_CP8_BEST.csv')
RF_PRE_BEST = pd.read_csv('C:/Users/Franck/Downloads/Result/RF_PRE_BEST.csv')
RF = [RF_FIFO_BEST,RF_MANUAL_BEST,RF_CP8_BEST,RF_PRE_BEST]
      
NeuralNetwork_FIFO_BEST = pd.read_csv('C:/Users/Franck/Downloads/Result/NeuralNetwork_FIFO_BEST.csv')
NeuralNetwork_MANUAL_BEST = pd.read_csv('C:/Users/Franck/Downloads/Result/NeuralNetwork_MANUAL_BEST.csv')
NeuralNetwork_CP8_BEST = pd.read_csv('C:/Users/Franck/Downloads/Result/NeuralNetwork_CP8_BEST.csv')
NeuralNetwork_PRE_BEST = pd.read_csv('C:/Users/Franck/Downloads/Result/NeuralNetwork_PRE_BEST.csv')
NeuralNetwork = [NeuralNetwork_FIFO_BEST,NeuralNetwork_MANUAL_BEST,NeuralNetwork_CP8_BEST,NeuralNetwork_PRE_BEST]


def extract_best(sub_dataset):
  index_cur = 0
  max = ['scaler', 0, 'arg']
  for index, row in sub_dataset.iterrows():
    if row['Acc'] > max[1]:
        max = [index, row['Acc'], row['Arg']]
  return max

def un_dict(dico):

  string = ''
  for item in dico.items():
    if isinstance(item[1], str):
      string = string + str(item[0]) + '= "' + str(item[1]) + '",'
    else:
      string = string + str(item[0]) + '= ' + str(item[1]) + ','
  return string

def select_sc(val):
  Scaler = StandardScaler()
  if(val == 0):
    Scaler = StandardScaler()
  if(val == 1):
    Scaler = MinMaxScaler()
  if(val == 2):
    Scaler = MaxAbsScaler()
  return Scaler

def select_model(index_mod):
  mod=GaussianNB()
  if(index_mod==0):
    mod=GaussianNB()
  if(index_mod==1):
    mod=KNeighborsClassifier()
  if(index_mod==2):
    mod=DecisionTreeClassifier()
  if(index_mod==3):
    mod=RandomForestClassifier()
  if(index_mod==4):
    mod=SVC()
  if(index_mod==5):
    mod=LogisticRegression()
  if(index_mod==6):
    mod=MLPClassifier()

  return mod

@profile(precision=4)
def TrainModel(labelColumn,array, index_mod):
  if isinstance(array[2], str):
    dicison = json.loads(array[2].replace("'", "\""))
  else:
    dicison = array[2]
  arg = un_dict(dicison)

  Scaler = select_sc(array[0])

  mod=select_model(index_mod)
  
  X = Scaler.fit_transform(temp_data.iloc[:, [0,1,2,3,8]])
  Y = temp_data.iloc[:, [labelColumn]].to_numpy().ravel()
  

  classifier = Pipeline([('scaler', Scaler), ('models', mod)])
  X_train, X_test, y_train, y_test = train_test_split(X, Y)

  y_pred = classifier.fit(X_test, y_test)

  prediction = classifier.predict([[37,12,1,15,0.728903]])

  return classifier

#[NAIVE, KNN, LR, SVM, DT, RF, NeuralNetwork]

fold_acc = [0,0,0,0]
fold_train = [0,0,0,0]
fold_predict = [0,0,0,0]

for shap in range(4):
  TrainModel(shap+4, extract_best(NeuralNetwork[shap]), 6)

