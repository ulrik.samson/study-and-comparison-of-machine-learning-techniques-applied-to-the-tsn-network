﻿
for (($i = 0); $i -lt 20 ; $i++)
{
    python.exe -m memory_profiler C:\Users\Franck\Downloads\test.py > C:\Users\Franck\Downloads\result.txt

    $fit = (Get-Content -Path C:\Users\Franck\Downloads\result.txt -TotalCount 24)[-1]
    $predict = (Get-Content -Path C:\Users\Franck\Downloads\result.txt -TotalCount 26)[-1]

    $fit = $fit.Replace('   150  ','')
    $fit = $fit.Replace('           4     y_pred = classifier.fit(X_test, y_test)','')

    $predict = $predict.Replace('   152  ','')
    $predict = $predict.Replace('           4     prediction = classifier.predict([[37,12,1,15,0.728903]])','')

    Add-Content C:\Users\Franck\Downloads\NeuralNetwork_fit.txt $fit
    Add-Content C:\Users\Franck\Downloads\NeuralNetwork_pred.txt $predict

    rm C:\Users\Franck\Downloads\result.txt
}