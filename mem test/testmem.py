from sklearn.svm import SVC
import tracemalloc, os, linecache
import numpy as np
import pandas as pd

def display_top(snapshot, key_type='lineno', limit=1):
    snapshot = snapshot.filter_traces((
        tracemalloc.Filter(False, "<frozen importlib._bootstrap>"),
        tracemalloc.Filter(False, "<unknown>"),
    ))
    top_stats = snapshot.statistics(key_type)

    print("Top %s lines" % limit)
    for index, stat in enumerate(top_stats[:limit], 1):
        frame = stat.traceback[0]
        # replace "/path/to/module/file.py" with "module/file.py"
        filename = os.sep.join(frame.filename.split(os.sep)[-2:])
        print("#%s: %s:%s: %.1f KiB"
              % (index, filename, frame.lineno, stat.size / 1024))
        line = linecache.getline(frame.filename, frame.lineno).strip()
        if line:
            print('    %s' % line)

    other = top_stats[limit:]
    if other:
        size = sum(stat.size for stat in other)
        print("%s other: %.1f KiB" % (len(other), size / 1024))
    total = sum(stat.size for stat in top_stats)
    print("Total allocated size: %.1f KiB" % (total / 1024))


temp_data = pd.read_csv('C:/Users/Franck/Downloads/Result/temp_data.csv')
X = temp_data.iloc[:, [0,1,2,3,8]]
y = temp_data.iloc[:, [4]].to_numpy().ravel()

tracemalloc.start()
neigh = SVC(C=1.2,kernel='poly', degree=40)
neigh.fit(X, y)
print(neigh.predict([[37,12,1,15,0.728903]]))
snapshot = tracemalloc.take_snapshot()

display_top(snapshot)